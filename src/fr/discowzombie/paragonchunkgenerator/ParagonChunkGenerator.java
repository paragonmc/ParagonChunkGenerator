package fr.discowzombie.paragonchunkgenerator;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.discowzombie.paragonchunkgenerator.events.AntiElytra;
import fr.discowzombie.paragonchunkgenerator.events.CraftEndCrystal;
import fr.discowzombie.paragonchunkgenerator.events.DragonHead;
import fr.discowzombie.paragonchunkgenerator.events.ShulkerDeath;

public class ParagonChunkGenerator extends JavaPlugin {
	
	@Override
	public void onEnable() {
		
		PluginManager pm = Bukkit.getServer().getPluginManager();
		pm.registerEvents(new CraftEndCrystal(), this);
		pm.registerEvents(new ShulkerDeath(), this);
		pm.registerEvents(new AntiElytra(), this);
		pm.registerEvents(new DragonHead(), this);
		
		super.onEnable();
	}
	
	@Override
	public void onDisable() {
		super.onDisable();
	}

}
