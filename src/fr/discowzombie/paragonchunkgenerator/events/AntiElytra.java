package fr.discowzombie.paragonchunkgenerator.events;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;

public class AntiElytra implements Listener {
	
	@EventHandler
	public void itemframe(PlayerInteractEntityEvent event){
		Entity e = event.getRightClicked();
		if(e instanceof ItemFrame){
			ItemFrame itf = (ItemFrame) e;
			if(itf.getItem().getType() == Material.ELYTRA){
				event.setCancelled(true);
				itf.setItem(new ItemStack(Material.AIR));
			}
		}
	}
	
	@EventHandler
    public void onItemSpawn(ItemSpawnEvent event) {
		if(event.getEntity().getItemStack().getType() == Material.ELYTRA){
			event.getEntity().remove();
		}
    }

}
