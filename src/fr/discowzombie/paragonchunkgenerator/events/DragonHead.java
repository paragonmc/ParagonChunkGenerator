package fr.discowzombie.paragonchunkgenerator.events;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class DragonHead implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		Block b = e.getBlock();
		if((b.getType() == Material.SKULL) || (b.getType() == Material.SKULL_ITEM) && (b.getData() == (byte)5)){
			e.setCancelled(true);
			b.setType(new ItemStack(Material.AIR).getType());
		}
	}

}
