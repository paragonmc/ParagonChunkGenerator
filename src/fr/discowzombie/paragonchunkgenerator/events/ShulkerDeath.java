package fr.discowzombie.paragonchunkgenerator.events;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class ShulkerDeath implements Listener {
	
	@EventHandler
	public void onShulker(EntityDeathEvent e){
		if(e.getEntityType().equals(EntityType.SHULKER)){
			e.getDrops().clear();
		}
	}

}
